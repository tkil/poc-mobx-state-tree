import React from "react";
import { render } from "react-dom";
import { MobxPocConnected } from "@components/mobxPoc/mobxPoc";

export const initReact = () => {
  const rootId = "root";
  const rootElement = document.getElementById(rootId);
  if (rootElement) {
    render(<MobxPocConnected />, rootElement);
  } else {
    console.error(`Could not find element with ID: ${rootId}`);
  }
};
