import React from "react";

import "./appRoot.css";

interface IProps {
  propData?: string;
}

export const AppRoot: React.FC<IProps> = (props: IProps) => {
  return (
    <div className="app-root" role="none">
      -- AppRoot Component --
    </div>
  );
};
