// NPM
import React from "react";
import { render } from "@testing-library/react";

// Project
import { AppRoot } from "../appRoot";

describe("Component: AppRoot", () => {
  describe("render()", () => {
    it("should render without error", () => {
      // Arrange
      const { baseElement } = render(<AppRoot />);
      // Act
      const element = baseElement.firstChild;
      // Assert
      expect(element).not.toBeNull();
    });
  });
});
