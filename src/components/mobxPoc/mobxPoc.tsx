import React from "react";
import { rootStore } from "@stores/rootStore";
import { observer } from "mobx-react";
import { values } from "mobx";

let id = 1;
const randomId = () => ++id;

interface ITodoProps {
  todo?: any;
}
const TodoView = observer((props: ITodoProps) => (
  <div>
    <input type="checkbox" checked={props.todo.done} onChange={(e) => props.todo.toggle()} />
    <input type="text" value={props.todo.name} onChange={(e) => props.todo.setName(e.target.value)} />
  </div>
));

interface ITodoCounterView {
  store: any;
}
const TodoCounterView = observer((props: ITodoCounterView) => (
  <div>
    {props.store.pendingCount} pending, {props.store.completedCount} completed
  </div>
));

interface IAppProps {
  store?: any;
}
export const MobxPoc = observer((props: IAppProps) => (
  <div>
    <button onClick={(e) => props.store.addTodo(randomId(), "New Task")}>Add Task</button>
    {values(props.store.todos).map((todo) => (
      <TodoView todo={todo} />
    ))}
    <TodoCounterView store={props.store} />
  </div>
));

export const MobxPocConnected: React.FC = () => <MobxPoc store={rootStore} />;
