// NPM
import React from "react";
import { render } from "@testing-library/react";

// Project
import { MobxPoc } from "../mobxPoc";

describe("Component: MobxPoc", () => {
  describe("render()", () => {
    it("should render without error", () => {
      // Arrange
      const { baseElement } = render(<MobxPoc />);
      // Act
      const element = baseElement.firstChild;
      // Assert
      expect(element).not.toBeNull();
    });
  });
});
