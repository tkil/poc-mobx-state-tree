import { types } from "mobx-state-tree";
import { values } from "mobx";

import { todoStore } from "./todoStore";
import { userStore } from "./userStore";

const RootStore = types
  .model({
    users: types.map(userStore),
    todos: types.optional(types.map(todoStore), {}),
  })
  .views((self) => ({
    get pendingCount() {
      return values(self.todos).filter((todo: any) => !todo.done).length;
    },
    get completedCount() {
      return values(self.todos).filter((todo: any) => todo.done).length;
    },
    getTodosWhereDoneIs(done: any) {
      return values(self.todos).filter((todo: any) => todo.done === done);
    },
  }))
  .actions((self) => ({
    addTodo(id: number, name: string) {
      self.todos.set(String(id), todoStore.create({ id, name }));
    },
  }));

export const rootStore = RootStore.create({
  users: {
    "1": {
      name: "mweststrate",
    },
    "2": {
      name: "mattiamanzati",
    },
    "3": {
      name: "johndoe",
    },
  },
  todos: {
    "1": {
      id: 0,
      name: "Ride a Bike",
      done: false,
    },
  },
});
